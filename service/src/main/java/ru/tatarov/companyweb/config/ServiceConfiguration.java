package ru.tatarov.companyweb.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.tatarov.companyweb.dao.config.DaoConfiguration;
import ru.tatarov.companyweb.service.DepartmentService;
import ru.tatarov.companyweb.service.PositionEmployeeService;

@Configuration
@Import(DaoConfiguration.class)
public class ServiceConfiguration {

    @Bean("departmentService")
    public DepartmentService departmentService(){
        return new DepartmentService();
    }

    @Bean
    public PositionEmployeeService positionEmployeeService(){
        return new PositionEmployeeService();
    }
}
