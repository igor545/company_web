package ru.tatarov.companyweb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tatarov.companyweb.dao.model.PositionEmployeeModel;
import ru.tatarov.companyweb.dao.repository.PositionEmployeeRepository;

import java.util.Collection;

@Service
public class PositionEmployeeService implements ServiceCRUD<PositionEmployeeModel, Long> {

    @Autowired
    private PositionEmployeeRepository positionEmployeeRepository;

    @Override
    public PositionEmployeeModel findById(Long aLong) {
        return null;
    }

    @Override
    public Collection<PositionEmployeeModel> findAll() {
        return null;
    }

    @Override
    public PositionEmployeeModel remove(Long aLong) {
        return null;
    }

    @Override
    public PositionEmployeeModel save(PositionEmployeeModel model) {
        return null;
    }

    @Override
    public PositionEmployeeModel update(PositionEmployeeModel model) {
        return null;
    }
}