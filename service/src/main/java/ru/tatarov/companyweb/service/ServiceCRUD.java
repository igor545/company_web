package ru.tatarov.companyweb.service;

import java.util.Collection;

public interface ServiceCRUD<T, ID> {
    T findById(ID id);

    Collection<T> findAll();

    T remove(ID id);

    T save(T model);

    T update(T model);
}
