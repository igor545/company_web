package ru.tatarov.companyweb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tatarov.companyweb.dao.model.DepartmentModel;
import ru.tatarov.companyweb.dao.repository.DepartmentRepository;

import java.util.Collection;


@Service
public class DepartmentService implements ServiceCRUD<DepartmentModel, Long> {
    @Autowired
    DepartmentRepository departmentRepository;

    @Override
    public DepartmentModel findById(Long aLong) {
        return departmentRepository.findById(aLong).orElse(null);
    }

    @Override
    public Collection<DepartmentModel> findAll() {
        return departmentRepository.findAll();
    }

    @Override
    public DepartmentModel remove(Long aLong) {
        return null;
    }

    @Override
    public DepartmentModel save(DepartmentModel model) {
        return null;
    }

    @Override
    public DepartmentModel update(DepartmentModel model) {
        return null;
    }
}
