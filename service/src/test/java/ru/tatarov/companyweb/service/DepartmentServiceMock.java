package ru.tatarov.companyweb.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.tatarov.companyweb.dao.repository.DepartmentRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class DepartmentServiceMock {
    @Mock
    public DepartmentRepository departmentRepositoryMock;

    @InjectMocks
    public DepartmentService departmentService;

//    @Test
//    void testFindById() {
//        Department expected = new Department(1L, "Отдел логистики");
//
//        when(departmentRepositoryMock.findById(anyLong())).thenReturn(Optional.of(expected));
//        Department actual = departmentService.findById(1L);
//
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    void testFindByName() {
//        Department expected = new Department(1L, "Отдел логистики");
//
//        when(departmentRepositoryMock.findByName(anyString())).thenReturn(Optional.of(expected));
//        Department actual = departmentService.findByName("Отдел логистики");
//
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    void testFindAll() {
//        Collection<Department> expected = new ArrayList<>();
//        expected.add(new Department(1L, "Отдел логистики"));
//        expected.add(new Department(2L, "Отдел продаж"));
//
//        when(departmentRepositoryMock.findAll()).thenReturn(expected);
//        Collection<Department> actual = departmentService.findAll();
//
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    void testRemove() {
//        when(departmentRepositoryMock.remove(anyLong())).thenReturn(true);
//        boolean isTrue = departmentService.remove(2L);
//
//        Assertions.assertTrue(isTrue);
//    }
//
//    @Test
//    void testSave() {
//        Department expected = new Department(2L, "Отдел продаж");
//
//        when(departmentRepositoryMock.save(any(Department.class))).thenReturn(expected);
//        Department actual = departmentService.save(expected);
//
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    void testUpdate() {
//        Department expected = new Department(3L,"Отдел продаж");
//
//        when(departmentRepositoryMock.update(any(Department.class))).thenReturn(expected);
//        Department actual = departmentService.update(expected);
//
//        Assertions.assertEquals(expected, actual);
//    }

}
