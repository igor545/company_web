package ru.tatarov.companyweb.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.tatarov.companyweb.dao.repository.PositionEmployeeRepository;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class PositionEmployeeServiceMock {

    @Mock
    public PositionEmployeeRepository positionEmployeeRepositoryMock;

    @InjectMocks
    public PositionEmployeeService positionEmployeeService;

//    @Test
//    void testFindById(){
//        PositionEmployeeModel expected = new PositionEmployeeModel(1L, "Новая позиция", "Отдел логистики");
//
//        when(positionEmployeeRepositoryMock.findById(anyLong())).thenReturn(Optional.of(expected));
//        PositionEmployeeModel actual = positionEmployeeService.findById(1L);
//
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    void testFindAll() {
//        Collection<PositionEmployeeModel> expected = new ArrayList<>();
//        expected.add(new PositionEmployeeModel(1L,"Новая позиция" ,"Отдел логистики"));
//        expected.add(new PositionEmployeeModel(2L,"Другая позиция", "Отдел продаж"));
//
//        when(positionEmployeeRepositoryMock.findAll()).thenReturn(expected);
//        Collection<PositionEmployeeModel> actual = positionEmployeeService.findAll();
//
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    void testRemove() {
//        when(positionEmployeeRepositoryMock.remove(anyLong())).thenReturn(true);
//        boolean isTrue = positionEmployeeService.remove(2L);
//
//        Assertions.assertTrue(isTrue);
//    }
//
//    @Test
//    void testSave() {
//        PositionEmployeeModel expected = new PositionEmployeeModel(2L,"Новая позиция", "Отдел продаж");
//
//        when(positionEmployeeRepositoryMock.save(any(PositionEmployeeModel.class))).thenReturn(expected);
//        PositionEmployeeModel actual = positionEmployeeService.save(expected);
//
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    void testUpdate() {
//        PositionEmployeeModel expected = new PositionEmployeeModel(3L,"Новая позиция","Отдел продаж");
//
//        when(positionEmployeeRepositoryMock.update(any(PositionEmployeeModel.class))).thenReturn(expected);
//        PositionEmployeeModel actual = positionEmployeeService.update(expected);
//
//        Assertions.assertEquals(expected, actual);
//    }
}
