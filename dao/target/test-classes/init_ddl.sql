CREATE TABLE IF NOT EXISTS department
(
    id   int PRIMARY KEY AUTO_INCREMENT,
    name varchar(20) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS position_employee
(
    id   int PRIMARY KEY AUTO_INCREMENT,
    name varchar(20) UNIQUE NOT NULL,
    department_id int,
    foreign key (department_id) references department(id)
);

CREATE TABLE IF NOT EXISTS employee
(
    id   int PRIMARY KEY AUTO_INCREMENT,
    position_employee_id int,
    name varchar(15) NOT NULL,
    surname varchar(15) NOT NULL,
    patronymic varchar(20),
    date_of_birth date,
    phone varchar(15),
    email varchar(30) NOT NULL,
    gender enum('MALE','FEMALE'),
    employee_manager_id int,
    foreign key (position_employee_id) references position_employee(id),
    foreign key (employee_manager_id) references employee(id)
);
