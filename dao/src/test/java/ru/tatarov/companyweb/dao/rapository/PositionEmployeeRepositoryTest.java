package ru.tatarov.companyweb.dao.rapository;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.tatarov.companyweb.dao.config.DaoConfiguration;
import ru.tatarov.companyweb.dao.repository.PositionEmployeeRepository;

import javax.sql.DataSource;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DaoConfiguration.class})
public class PositionEmployeeRepositoryTest {
    @Autowired
    DataSource dataSource;
    @Autowired
    private PositionEmployeeRepository positionEmployeeRepository;

//    @BeforeEach
//    public void fillTable() {
//        Connection connection = null;
//        try {
//            connection = dataSource.getConnection();
//        } catch (SQLException e) {
//            throw new CompanyDaoException("Error connecting to database!");
//        }
//        try {
//            RunScript.execute(connection, new FileReader("src/test/resources/init_ddl.sql"));
//            RunScript.execute(connection, new FileReader("src/test/resources/fill_data_ddl.sql"));
//            connection.close();
//        } catch (SQLException | FileNotFoundException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @AfterEach
//    public void dropTable() {
//        Connection connection = null;
//        try {
//            connection = dataSource.getConnection();
//        } catch (SQLException e) {
//            throw new CompanyDaoException("Error connecting to database!");
//        }
//        try {
//            RunScript.execute(connection, new FileReader("src/test/resources/delete_ddl.sql"));
//            RunScript.execute(connection, new FileReader("src/test/resources/drop_ddl.sql"));
//            connection.close();
//        } catch (SQLException | FileNotFoundException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    @DisplayName("Testing method save")
//    public void createTest() {
//        PositionEmployeeModel expected = new PositionEmployeeModel( null, "Новая позиция", "Отдел логистики");
//        positionEmployeeRepository.save(expected);
//        expected.setId(3L);
//        PositionEmployeeModel actual = positionEmployeeRepository.findById(3L).orElse(null);
//        Assertions.assertNotNull(actual);
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    @DisplayName("Testing method findById")
//    public void findByIdTest() {
//        PositionEmployeeModel expected = new PositionEmployeeModel( 1L, "Водитель", "Отдел логистики");
//        expected.setId(2L);
//        PositionEmployeeModel actual = positionEmployeeRepository.findById(2L).orElse(null);
//        Assertions.assertNotNull(actual);
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    @DisplayName("Testing method findAll")
//    public void findAllTest() {
//        Collection<PositionEmployeeModel> expected = new ArrayList<>();
//        expected.add(new PositionEmployeeModel(1L, "Генеральный директор", null));
//        expected.add(new PositionEmployeeModel(2L, "Водитель", "Отдел логистики"));
//        Collection<PositionEmployeeModel> actual = positionEmployeeRepository.findAll();
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    public void updateTest() {
//        PositionEmployeeModel expected = positionEmployeeRepository.findById(2L).orElse(null);
//        Assertions.assertNotNull(expected);
//        expected.setName("Update");
//        expected.setDepartment("Отдел логистики");
//        positionEmployeeRepository.update(expected);
//        PositionEmployeeModel actual = positionEmployeeRepository.findById(2L).orElse(null);
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    @DisplayName("Testing method remove")
//    public void deleteTest() {
//        PositionEmployeeModel positionEmployee = new PositionEmployeeModel( null, "Новая позиция", "Отдел логистики");
//        positionEmployeeRepository.save(positionEmployee);
//        boolean actual = positionEmployeeRepository.remove(3L);
//        Assertions.assertTrue(actual);
//    }
}
