package ru.tatarov.companyweb.dao.rapository;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.tatarov.companyweb.dao.config.DaoConfiguration;
import ru.tatarov.companyweb.dao.repository.EmployeeRepository;
import ru.tatarov.companyweb.dao.repository.PositionEmployeeRepository;

import javax.sql.DataSource;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DaoConfiguration.class})
public class EmployeeRepositoryTest {
    @Autowired
    DataSource dataSource;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private PositionEmployeeRepository positionEmployeeRepository;

//    @BeforeEach
//    public void fillTable() {
//        Connection connection = null;
//        try {
//            connection = dataSource.getConnection();
//        } catch (SQLException e) {
//            throw new CompanyDaoException("Error connecting to database!");
//        }
//        try {
//            RunScript.execute(connection, new FileReader("src/test/resources/init_ddl.sql"));
//            RunScript.execute(connection, new FileReader("src/test/resources/fill_data_ddl.sql"));
//            connection.close();
//        } catch (SQLException | FileNotFoundException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @AfterEach
//    public void dropTable() {
//        Connection connection = null;
//        try {
//            connection = dataSource.getConnection();
//        } catch (SQLException e) {
//            throw new CompanyDaoException("Error connecting to database!");
//        }
//        try {
//            RunScript.execute(connection, new FileReader("src/test/resources/delete_ddl.sql"));
//            RunScript.execute(connection, new FileReader("src/test/resources/drop_ddl.sql"));
//            connection.close();
//        } catch (SQLException | FileNotFoundException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    @DisplayName("Testing method save")
//    public void createTest() {
//        PositionEmployeeModel positionEmployee = positionEmployeeRepository.findById(2L).orElse(null);
//        Assertions.assertNotNull(positionEmployee);
//        LocalDate localDate = LocalDate.of(1995,5,15);
//        EmployeeModel expected = new EmployeeModel(null, positionEmployee, "Петр","Петров","Петрович",
//                localDate,"89228445914","petr@bk.ru", Gender.valueOf("MALE"),1L);
//        employeeRepository.save(expected);
//        EmployeeModel actual = employeeRepository.findById(3L).orElse(null);
//        Assertions.assertNotNull(actual);
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    @DisplayName("Testing method findById")
//    public void findByIdTest() {
//        LocalDate localDate = LocalDate.of(1997,8,8);
//        PositionEmployeeModel positionEmployee = new PositionEmployeeModel(2L, "Водитель", "Отдел логистики");
//        EmployeeModel expected = new EmployeeModel(2L,positionEmployee, "Ирина", "Дмитриева",
//                "Петровна",localDate , "89175754111", "ira@gmail.com", Gender.valueOf("FEMALE"),1L);
//        EmployeeModel actual = employeeRepository.findById(2L).orElse(null);
//        Assertions.assertNotNull(actual);
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    @DisplayName("Testing method findAll")
//    public void findAllTest() {
//        Collection<EmployeeModel> expected = new ArrayList<>();
//        PositionEmployeeModel positionEmployeeDriver = positionEmployeeRepository.findById(2L).orElse(null);
//        Assertions.assertNotNull(positionEmployeeDriver);
//        PositionEmployeeModel positionEmployeeDirector = positionEmployeeRepository.findById(1L).orElse(null);
//        Assertions.assertNotNull(positionEmployeeDirector);
//        LocalDate localDate = LocalDate.of(1970,10,21);
//        LocalDate localDate2 = LocalDate.of(1997,8,8);
//        expected.add(new EmployeeModel(1L,positionEmployeeDirector, "Петр", "Петров", "Петрович",
//                localDate, "89165554433", "petr4@mail.ru", Gender.valueOf("MALE"),0L));
//        expected.add(new EmployeeModel(2L, positionEmployeeDriver,"Ирина", "Дмитриева", "Петровна",
//                localDate2, "89175754111", "ira@gmail.com", Gender.valueOf("FEMALE"),1L));
//        Collection<EmployeeModel> actual = employeeRepository.findAll();
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    public void updateTest() {
//        EmployeeModel expected = employeeRepository.findById(2L).orElse(null);
//        Assertions.assertNotNull(expected);
//        expected.setName("Update");
//        employeeRepository.update(expected);
//        EmployeeModel actual = employeeRepository.findById(2L).orElse(null);
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    @DisplayName("Testing method remove")
//    public void deleteTest() {
//        boolean actual = employeeRepository.remove(2L);
//        Assertions.assertTrue(actual);
//    }
}
