package ru.tatarov.companyweb.dao.rapository;

import org.h2.tools.RunScript;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.tatarov.companyweb.dao.config.DaoConfiguration;
import ru.tatarov.companyweb.dao.exception.CompanyDaoException;
import ru.tatarov.companyweb.dao.repository.DepartmentRepository;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DaoConfiguration.class})
public class DepartmentRepositoryTest {
    @Autowired
    DataSource dataSource;
    @Autowired
    private DepartmentRepository departmentRepository;

//    @BeforeEach
//    public void fillTable() {
//        Connection connection;
//        try {
//            connection = dataSource.getConnection();
//        } catch (SQLException e) {
//            throw new CompanyDaoException("Error connecting to database!");
//        }
//        try {
//            RunScript.execute(connection, new FileReader("src/test/resources/init_ddl.sql"));
//            RunScript.execute(connection, new FileReader("src/test/resources/fill_data_ddl.sql"));
//            connection.close();
//        } catch (SQLException | FileNotFoundException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @AfterEach
//    public void dropTable() {
//        Connection connection;
//        try {
//            connection = dataSource.getConnection();
//        } catch (SQLException e) {
//            throw new CompanyDaoException("Error connecting to database!");
//        }
//        try {
//            RunScript.execute(connection, new FileReader("src/test/resources/delete_ddl.sql"));
//            RunScript.execute(connection, new FileReader("src/test/resources/drop_ddl.sql"));
//            connection.close();
//        } catch (SQLException | FileNotFoundException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    @DisplayName("Testing method save")
//    public void createTest() {
//        Department expected = new Department(null, "Новый отдел");
//        departmentRepository.save(expected);
//        Department actual = departmentRepository.findById(3L).orElse(null);
//        Assertions.assertNotNull(actual);
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    @DisplayName("Testing method findById")
//    public void findByIdTest() {
//        Department expected = new Department(null, "Юридический отдел");
//        expected.setId(2L);
//        Department actual = departmentRepository.findById(2L).orElse(null);
//        Assertions.assertNotNull(actual);
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    @DisplayName("Testing method findByName")
//    public void findByName() {
//        Department expected = new Department(null, "Юридический отдел");
//        expected.setId(2L);
//        Department actual = departmentRepository.findByName("Юридический отдел").orElse(null);
//        Assertions.assertNotNull(actual);
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    @DisplayName("Testing method findAll")
//    public void findAllTest() {
//        Collection<Department> expected = new ArrayList<>();
//        expected.add(new Department(1L, "Отдел логистики"));
//        expected.add(new Department(2L, "Юридический отдел"));
//        Collection<Department> actual = departmentRepository.findAll();
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    public void updateTest() {
//        Department expected = departmentRepository.findById(2L).orElse(null);
//        Assertions.assertNotNull(expected);
//        expected.setName("Update");
//        departmentRepository.update(expected);
//        Department actual = departmentRepository.findById(2L).orElse(null);
//        Assertions.assertEquals(expected, actual);
//    }
//
//    @Test
//    @DisplayName("Testing method remove")
//    public void deleteTest() {
//        boolean actual = departmentRepository.remove(2L);
//        Assertions.assertTrue(actual);
//    }
}