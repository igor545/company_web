INSERT INTO department (name)
VALUES ('Отдел логистики'),
       ('Юридический отдел');

INSERT INTO position_employee (name, department_id)
VALUES ('Генеральный директор', null),
       ('Водитель', 1);

INSERT INTO employee (position_employee_id, name, surname, patronymic, date_of_birth, phone, email, gender,
                      employee_manager_id)
VALUES (1, 'Петр', 'Петров', 'Петрович', '1970-10-21', 89165554433, 'petr4@mail.ru', 'MALE', null),
       (2, 'Ирина', 'Дмитриева', 'Петровна', '1997-08-08', 89175754111, 'ira@gmail.com', 'FEMALE', 1);