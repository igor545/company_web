package ru.tatarov.companyweb.dao.model;

public enum Gender {
    MALE, FEMALE
}