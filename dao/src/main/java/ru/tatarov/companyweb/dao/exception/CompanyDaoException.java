package ru.tatarov.companyweb.dao.exception;

public class CompanyDaoException extends RuntimeException{

    public CompanyDaoException(String message) {
        super(message);
    }

    public CompanyDaoException(String message, Throwable cause) {
        super(message, cause);
    }

    public CompanyDaoException(Throwable cause) {
        super(cause);
    }
}
