package ru.tatarov.companyweb.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tatarov.companyweb.dao.model.PositionEmployeeModel;

public interface PositionEmployeeRepository extends JpaRepository<PositionEmployeeModel, Long> {
}