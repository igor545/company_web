package ru.tatarov.companyweb.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.tatarov.companyweb.dao.model.EmployeeModel;


public interface EmployeeRepository extends JpaRepository<EmployeeModel, Long> {
}