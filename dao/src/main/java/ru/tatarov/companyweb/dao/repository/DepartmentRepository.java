package ru.tatarov.companyweb.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tatarov.companyweb.dao.model.DepartmentModel;

public interface DepartmentRepository extends JpaRepository<DepartmentModel, Long> {
    DepartmentModel findByName(String name);
}