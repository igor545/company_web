package ru.tatarov.companyweb.dao;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tatarov.companyweb.dao.config.DaoConfiguration;
import ru.tatarov.companyweb.dao.model.EmployeeModel;
import ru.tatarov.companyweb.dao.model.PositionEmployeeModel;
import ru.tatarov.companyweb.dao.repository.EmployeeRepository;
import ru.tatarov.companyweb.dao.repository.PositionEmployeeRepository;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(DaoConfiguration.class);
        //PositionEmployeeRepository PositionEmployeeRepository = (PositionEmployeeRepository) applicationContext.getBean("positionEmployeeRepository");
        EmployeeRepository employeeRepository = (EmployeeRepository) applicationContext.getBean("employeeRepository");
        //Department department = departmentService.findById(1L);
        //ArrayList<PositionEmployeeModel> employeeModels = (ArrayList<PositionEmployeeModel>) PositionEmployeeRepository.findAll();
        ArrayList<EmployeeModel> employeeModels = (ArrayList<EmployeeModel>) employeeRepository.findAll();
        for (EmployeeModel employeeModel : employeeModels) {
            System.out.println(employeeModel+"\n");
        }

    }
}
