package ru.tatarov.companyweb.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.tatarov.companyweb.dao.model.DepartmentModel;
import ru.tatarov.companyweb.service.DepartmentService;

import java.util.Collection;

@Controller
@RequestMapping("department")
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;

//    private final DepartmentService departmentService;   // этот вариант предлагает идея?
//
//    @Autowired
//    public DepartmentController(DepartmentService departmentService) {
//        this.departmentService = departmentService;
//    }

    @GetMapping("/departments")
    public String findAll(Model model){
        Collection<DepartmentModel> collections = departmentService.findAll();
        model.addAttribute("departments", collections);
        return "department_list";
    }

    @GetMapping("/department_add")
    public String addDepartmentForm(DepartmentModel department){
        return "department_add";
    }

    @PostMapping(value = "/department_add", produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveDepartment(@RequestBody DepartmentModel department, Model model) {
        DepartmentModel saveDepartment= departmentService.save(department);
        model.addAttribute("name", saveDepartment);
        return "redirect:/department/departments";
    }

    @GetMapping("department_delete/{id}")
    public String deleteDepartment(@PathVariable("id") Long id){
        departmentService.remove(id);
        return "redirect:/department/departments";
    }

    @GetMapping("department_update/{id}")
    public String updateDepartmentForm(@PathVariable("id") Long id, Model model){
        DepartmentModel department = departmentService.findById(id);
        model.addAttribute("department", department);
        return "department_update";
    }

    @PostMapping("department_update")
    public String updateDepartment(DepartmentModel department){
        departmentService.update(department);
        return "redirect:/department/departments";
    }

}
